//
//  SharedObject.swift
//  Autometrics
//
//  Created by Entrac Solutions on 07/06/20.
//  Copyright © 2020 Gangaraju. All rights reserved.
//

import UIKit


final class SharedObject: NSObject {
   static let sharedInstance = SharedObject()
   var userRoles:[String] = []

   private override init() { }

   func foo() { }
}
