//
//  DashBoardData.swift
//  Autometrics
//
//  Created by Gangaraju on 29/05/20.
//  Copyright © 2020 Gangaraju. All rights reserved.
//

import UIKit

struct Message: Codable {
    let deptCode:String
    let deptName:String
    let totalActiveCount:Int
    let criticalRange:CriticalRange
    let delyaedRange:DelayedRange
    let ontimeRange:OntimeRange
    let delayCounts:DelayedCounts
    let criticalTptList:[CriticalTptList]
    //let criticalData:CriticalData
    
    private enum CodingKeys: String, CodingKey {
        case deptCode = "dept_code"
        case deptName = "dept_name"
        case totalActiveCount = "total_active_count"
        case criticalRange = "Critical_range"
        case delyaedRange = "Delayed_range"
        case ontimeRange = "OnTime_range"
        case delayCounts = "delay_counts"
        case criticalTptList = "critical_tpt_list"
       // case criticalData = "critical_data"
    }
}



struct DelayedRange: Codable {
    let  max:String
    let  min:String
   
}
struct OntimeRange: Codable {
    let  max:String
    let  min:String
    
}


struct CriticalRange: Codable {
    let  max:String
    let  min:String
}
class CriticalListGroup: NSObject {
    var  transporters_name:String = "";
    var  consigner_code:String = ""
    var  max:Int = 0
    var  min:Int = -1
    var  len:Int = 0
    var criticalList:[CriticalTptList]  = []
}
struct DelayedCounts: Codable {
    let  critical:Int
    let  delayed:Int
    let  onTime:Int
    
    private enum CodingKeys: String, CodingKey {
        case critical = "Critical"
        case delayed = "Delayed"
        case onTime = "OnTime"
    }
}


    
  
  
 

struct CriticalTptList: Codable {
    let consigner_code:String
    let delay_type:String
    let transporters_name:String
    let  max:String
    let  min:String
    let  len:String
}


struct DashBoardData: Codable {
    let message: [Message]
    let status:String

}


