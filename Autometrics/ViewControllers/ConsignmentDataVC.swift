//
//  ConsignmentDataVC.swift
//  Autometrics
//
//  Created by Khaja Bee on 06/06/20.
//  Copyright © 2020 Gangaraju. All rights reserved.
//

import UIKit

extension ConsignmentDataVC:UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController) {
        let searchText:String = searchController.searchBar.text!;
       
      //print(self.viewModel.deptWiseDatataArray)
        if(searchText.count>0){
            self.filteredArray = self.viewModel.consignmentDatataArray.filter { $0.consignee_name.localizedCaseInsensitiveContains(searchText) || $0.consignee_city.localizedCaseInsensitiveContains(searchText) || "\($0.spocdetails[0].spoc_contact_number)".localizedCaseInsensitiveContains(searchText) || $0.expected_trip_end.localizedCaseInsensitiveContains(searchText) || $0.consignment_code.localizedCaseInsensitiveContains(searchText) || $0.spocdetails[0].plant_location.localizedCaseInsensitiveContains(searchText)/* || $0.transit_delay_days.localizedCaseInsensitiveContains(searchText) */}
         
        }else{
            self.filterDaata()
        }
        print(self.filteredArray);
        self.tableView.reloadData()
    
    }
       
}

class ConsignmentDataVC: UITableViewController, UISearchBarDelegate {

    var viewModel: ConsignmentDataViewModel!
    var filteredArray:[Consignments] = []
    var transporterName:String = ""
    var deptCode:String = ""
    var deptName:String = ""
    var resultSearchController = UISearchController()

    @IBAction func phoneBtnAction(_ sender: UIButton) {
        let bttnTag = sender.tag
        let  consignmentData = self.viewModel.consignmentDatataArray[bttnTag]
        
        print(bttnTag)
        
        
       // let phoneNumber:String = (sender.titleLabel?.text!)!
        if let url = URL(string: "tel://\(consignmentData.spocdetails[0].spoc_contact_number)"),
        UIApplication.shared.canOpenURL(url) {
           if #available(iOS 10, *) {
             UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            print("Dont have this future in this device")
                 // add error message here
        }
    }
    func showAlert(alertMessage:String,title:String) {
        let alert = UIAlertController(title: title, message: alertMessage,         preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { _ in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.sectionHeaderHeight = 60
        self.tableView.sectionFooterHeight = 70
        viewModel = ConsignmentDataViewModel()

        //register tableview footer
        self.tableView.register(UINib(nibName: "ConsignmentCellFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: "ConsignmentCellFooterView")
        
       resultSearchController = ({
            let searchController = UISearchController(searchResultsController: nil)
            searchController.searchResultsUpdater = self
            searchController.dimsBackgroundDuringPresentation = false
            searchController.searchBar.sizeToFit()
            searchController.searchBar.backgroundColor = UIColor.white;
        
        
        //searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true

        
       // self.tableView.tableHeaderView = controller.searchBar
         // Place the search bar in the navigation bar.
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            searchController.searchBar.autocapitalizationType = .none
            searchController.dimsBackgroundDuringPresentation = false
            searchController.searchBar.delegate = self // Monitor when the search button is tapped.
            searchController.searchBar.backgroundColor = UIColor.white
            self.navigationItem.searchController = searchController


            
            // Make the search bar always visible.
            navigationItem.hidesSearchBarWhenScrolling = false
          
            definesPresentationContext = true
            
        } else {
            // Fallback on earlier versions
        }
      
           
        return searchController
        })()
        
        if #available(iOS 11.0, *) {
           // resultSearchController.searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor.gray]
       UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        
        self.tableView.reloadData()
        
        //show loading Indicator
//         DispatchQueue.main.async {
//            LoadingOverlay.shared.showOverlay(view: UIApplication.shared.keyWindow!)
//        }
//        let last10DaysDates = Date.getDates(forLastNDays: 10)
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd"
//        let dictionary = ["dept_code": "SNDG", "endDate": dateFormatter.string(from: Date()), "startDate": last10DaysDates.last as Any] as [String : Any]"
        
 //       let dictionary = ["dept_code": "LOG-PRT"]
//        viewModel.fetchConsignmentData(inputData: dictionary, completion: { (isSuccess) in
//            if isSuccess {
//                //remove loading indcator
//                DispatchQueue.main.async {
//                    LoadingOverlay.shared.hideOverlayView()
//                    self.filterDaata()
//                    print("Count....")
//                    print(self.viewModel.consignmentDatataArray.count)
//                    self.tableView.reloadData()
//                }
//            }else{
//                self.showAlert(alertMessage: "ConsignmentData Failed", title: "")
//
//            }
//        })
    }
    
    func filterDaata(){
        //transport name wise search
        if(self.transporterName == self.title){
            if  (!resultSearchController.isActive) {
                self.viewModel.consignmentDatataArray = self.viewModel.consignmentDatataArray.filter { $0.transporter_name == self.transporterName }
                           
            }
           
            self.filteredArray = self.viewModel.consignmentDatataArray.filter { $0.transporter_name == self.title
                
            }
        }else{
            //transport name and consigner_code wise search
            if  (!resultSearchController.isActive) {
                
                 self.viewModel.consignmentDatataArray = self.viewModel.consignmentDatataArray.filter { $0.consigner_code == self.title && $0.transporter_name == self.transporterName  }
                
            }
             self.filteredArray = self.viewModel.consignmentDatataArray.filter {
                $0.consigner_code == self.title && $0.transporter_name == self.transporterName
            }
           
        }
    
        //Decending oreder
//    self.viewModel.consignmentDatataArray = self.viewModel.consignmentDatataArray.sorted {
//                     $0.transit_delay_days > $1.transit_delay_days
//                 }
//    self.filteredArray = self.filteredArray.sorted {
//                   $0.transit_delay_days > $1.transit_delay_days
//        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
            
        return 10
//        if  (resultSearchController.isActive) {
//            return self.filteredArray.count
//        } else {
//            return self.viewModel.consignmentDatataArray.count
//        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
   
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 240
       }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {


//        var  consignmentData = self.viewModel.consignmentDatataArray[section]
//        if  (resultSearchController.isActive) {
//            consignmentData = self.filteredArray[section]
//        }

        let headerBgView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width , height: 60))
         headerBgView.backgroundColor  = UIColor(red: 46/255, green: 59/255, blue: 74/255, alpha: 1)
        
        
        let headerView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width-0 , height: 60))
        headerView.backgroundColor = UIColor(red: 46/255, green: 59/255, blue: 74/255, alpha: 1)
      

        
        let firstColumnWidth = (headerView.frame.size.width/2)-25
        
        let truckNoHeaderLbl: UILabel = UILabel.init(frame: CGRect(x: 8, y:5, width: firstColumnWidth, height: 24))
        truckNoHeaderLbl.font = UIFont.systemFont(ofSize: 13)
        truckNoHeaderLbl.textColor = UIColor.white
        truckNoHeaderLbl.backgroundColor = UIColor.clear
//        if(self.deptName == "Spare Parts (Courier)"){
//            truckNoHeaderLbl.text = "Consiignments No"
//
//        }else{
//            truckNoHeaderLbl.text = "Truck No"
//
//        }
        truckNoHeaderLbl.text = "Truck No"
        
        let truckNoLbl: UILabel = UILabel.init(frame: CGRect(x: 8, y: 30, width: firstColumnWidth, height: 25))
        truckNoLbl.font = UIFont.systemFont(ofSize: 16)
        truckNoLbl.textColor = UIColor.white
        truckNoLbl.backgroundColor = UIColor.clear
        //truckNoLbl.text = consignmentData.consignment_code
        truckNoLbl.text = "NL01AA9609"
        
        let secondtColunWidth = (headerView.frame.size.width/2)-8
        
        let loadingPlanetHeaderLbl: UILabel = UILabel.init(frame: CGRect(x: truckNoHeaderLbl.frame.size.width+25, y: 5, width: secondtColunWidth, height: 24))
        loadingPlanetHeaderLbl.font = UIFont.systemFont(ofSize: 13)
        loadingPlanetHeaderLbl.textColor = UIColor.white
        loadingPlanetHeaderLbl.backgroundColor = UIColor.clear
        loadingPlanetHeaderLbl.textAlignment = NSTextAlignment.right
        loadingPlanetHeaderLbl.text = "Loading Plant"
        
        
        let loadingPlanetLbl: UILabel = UILabel.init(frame: CGRect(x: truckNoHeaderLbl.frame.size.width+25, y: 30, width: secondtColunWidth, height: 25))
        loadingPlanetLbl.font = UIFont.systemFont(ofSize: 16)
        loadingPlanetLbl.textColor = UIColor.white
        loadingPlanetLbl.backgroundColor = UIColor.clear
        loadingPlanetLbl.textAlignment = NSTextAlignment.right
        loadingPlanetLbl.text = "Gurgaon"
        
        //loadingPlanetLbl.text = consignmentData.spocdetails[0].plant_location

        
        headerView.addSubview(truckNoHeaderLbl)
        headerView.addSubview(truckNoLbl)
        headerView.addSubview(loadingPlanetHeaderLbl)
        headerView.addSubview(loadingPlanetLbl)
        headerBgView.addSubview(headerView)
        return headerBgView
        
    }

    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {

        if let footerView: ConsignmentCellFooterView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ConsignmentCellFooterView") as? ConsignmentCellFooterView {
            updateFooterView(number: 6, footerView: footerView)
            return footerView
        }
        return UIView()
    }
    
    func updateFooterView(number: Int, footerView: ConsignmentCellFooterView) {
        footerView.footerStackView.subviews.forEach { $0.removeFromSuperview() }
        for _ in 0..<number {
            let button = UIButton(type: .system)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.setImage(UIImage(named: "phoneIcon"), for: .normal)
            button.tintColor = .white
            button.backgroundColor = UIColor(red: 46/255, green: 59/255, blue: 74/255, alpha: 1)
            footerView.footerStackView.addArrangedSubview(button)
        }
    }
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! ConsignmentDataCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none

//        var  consignmentData = self.viewModel.consignmentDatataArray[indexPath.section]
//        if  (resultSearchController.isActive) {
//            consignmentData = self.filteredArray[indexPath.section]
//        }
        
        cell.consigneeNameLbl?.text = "JAIN UDYOG BLUE HORSES  BLUE HORSES "   //consignmentData.consignee_name
        cell.destinationCityLbl?.text = "Silchar"    // consignmentData.consignee_city


        cell.transitDelayLbl?.text = ""              //consignmentData.transit_delay_days
        cell.originalEtaLbl?.text = "06-06-2020"               // consignmentData.expected_trip_end
       // cell.originalEtaLbl?.text = self.formattedDateFromString(dateStr: consignmentData.expected_trip_end)
        //cell.phoneCallBtn?.setTitle("\(consignmentData.spocdetails[0].spoc_contact_number)", for: .normal)
        cell.spocLbl?.text = "BLUE HORSES BLUE HORSESBLUE HORSES "       //consignmentData.spocdetails[0].spoc_contact_name
        cell.currentCityNameLbl.text = "Kokrajhar/Assam"
//        if("\(consignmentData.spocdetails[0].spoc_contact_number)".count>0){
//            cell.phoneCallBtn?.isHidden = false;
//        }else{
//            cell.phoneCallBtn?.isHidden = true;
//        }
        //cell.remarksLbl?.text = consignmentData.delay_reason
        
        return cell
    }
    

    func formattedDateFromString(dateStr:String) -> String{
        let dateFormatter = DateFormatter();
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let convertedDate = dateFormatter.date(from: dateStr){
            
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = "dd/MM/yyyy";
            return outputFormatter.string(from: convertedDate)
            
        }
        return "";
        
    }
}

//extension Date {
//    static func getDates(forLastNDays nDays: Int) -> [String] {
//        let cal = NSCalendar.current
//        // start with today
//        var date = cal.startOfDay(for: Date())
//        var arrDates = [String]()
//        for _ in 1 ... nDays {
//            // move back in time by one day:
//            date = cal.date(byAdding: Calendar.Component.day, value: -1, to: date)!
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "yyyy-MM-dd"
//            let dateString = dateFormatter.string(from: date)
//            arrDates.append(dateString)
//        }
//        return arrDates
//    }
//}
