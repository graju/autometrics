//
//  DashboardViewModel.swift
//  Autometrics
//
//  Created by Gangaraju on 29/05/20.
//  Copyright © 2020 Gangaraju. All rights reserved.
//

import UIKit

struct DashBoardDatarequest: RequestType{
    var data: RequestData
    typealias ResponseType = DashBoardData
}

class DashBoardViewModel: NSObject {
    public var dashBordDataArray: [Message] = []
    typealias FetchDashBoardCompletion = ((_ isSuccess: Bool) -> Void)
    
    func fetchDashBordData(completion: @escaping FetchDashBoardCompletion){
        
        let dashBordrequest = DashBoardDatarequest(data: RequestData(method: .post, url: url.dashboard.rawValue))
        
        dashBordrequest.serviceCall(
            onSuccess: { (dashBordResponseData: DashBoardData) in
                DispatchQueue.main.async {
                    LoadingOverlay.shared.hideOverlayView()
                }
                self.dashBordDataArray = dashBordResponseData.message;
                completion(true)
        },
            onError: { (error: Error) in
                DispatchQueue.main.async {
                    LoadingOverlay.shared.hideOverlayView()
                }
                switch error.localizedDescription {
                case ErrorMessages.noConnection.rawValue:
                    print("Error");
                //self.fetchOfflineData(completion: completion)
                default:
                    print(error.localizedDescription)
                }
                
                
        })
    }
}

