//
//  ConsignmentData.swift
//  Autometrics
//
//  Created by Khaja Bee on 06/06/20.
//  Copyright © 2020 Gangaraju. All rights reserved.
//

import UIKit

struct ConsignmentsData: Codable {
    let consignments: [Consignments]
    let counters: CountersData
}

struct Consignments: Codable {
    let _id:String
    let timestamp:String
    let truck_no:String
    let actual_lspuser:String
    let modified_date:String
    let created_date:String
    let transporter_code:String
    let transporter_name:String
    let dept_code:String
    let consignee_code:String
    let consignment_code:String
    let cluster:String
    let consignee_city:String
    let consignee_name:String
    let consigner_code:String
    let items:String
    let parent_transporter_code:String
    let status:Int
    let vehicle_mode:String
    let invoice_time:String
    let gate_in_time:String
    let gate_out_time:String
    let inside_fence:String
    let outside_fence:String
    let distance_in_km:Int
    let distance_per_leg:Int
    let expected_trip_end:String
    let revised_trip_end:String
    let time_per_leg:Int
    let transit_time:Int
    let distance_from_dealer_location: Double
    let loading_delay_exception: Int
    let transporter: [TransporterData]
    let truck: [TruckData]
    let spocdetails: [SpocDetailsData]
}

struct TransporterData: Codable {
    var transporter_name:String
}

struct TruckData: Codable {
    let timestamp:String
    let actual_lspuser:String
    let modified_date:String
    let area:String
    let city:String
    let state:String
}

struct SpocDetailsData: Codable {
    let _id:String
    let dept_code:String
    let plant_location:String
    let parent_transporter_code:String
    let spoc_contact_name:String
    let spoc_contact_number:Int
    let transporter_name:String
    let spoc_email_id:String
}

struct CountersData: Codable {
    let transit_delay: [TransitDelayData]
    let no_gps: [NoGpsData]
    let nogpslifetime: [NoGpsLifeTimeData]
    let overspeeding: [overspeedingData]
    let night_drive: [NightDriveData]
    let enroute_stoppage: [EnrouteStoppageData]
    let loading_delay: [LoadingDelayData]
}

struct TransitDelayData: Codable {
    let _id: Int
    let transit_delay_count: Int
}

struct NoGpsData: Codable {
    let _id: Int
    let no_gps_data_count: Int
}

struct NoGpsLifeTimeData: Codable {
    let _id: Int
    let no_gps_data_lifetime_count: Int
}

struct overspeedingData: Codable {
    let _id: Int
    let overspeeding_exception_count: Int
}

struct NightDriveData: Codable {
    let _id: Int
    let nightdriving_exception_count: Int
}

struct EnrouteStoppageData: Codable {
    let _id: Int
    let enroute_exception_count: Int
}

struct LoadingDelayData: Codable {
    let _id: Int
    let loading_delay_exception_count: Int
}
