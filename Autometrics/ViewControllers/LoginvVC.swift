//
//  LoginvVC.swift
//  Autometrics
//
//  Created by Gangaraju on 27/05/20.
//  Copyright © 2020 Gangaraju. All rights reserved.
//

import UIKit

/*struct UserDetails {
    let username: String
    let password: String
    
    init(_ json: [String: String]) {
        self.username = json["username"] ?? ""
        self.password = json["password"] ?? ""
    }
}*/

extension UITextField {
    func addBottomBorder(){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
        bottomLine.backgroundColor = UIColor.gray.cgColor
        borderStyle = .none
        layer.addSublayer(bottomLine)
    }
}

extension LoginvVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       textField.resignFirstResponder()
        return false;
    }
}

class LoginvVC: UIViewController {
    @IBOutlet weak var loginBgView: UIView?
    @IBOutlet weak var emailTextField: UITextField?
    @IBOutlet weak var pwdTextField: UITextField?
    @IBOutlet weak var loginButton: UIButton?
    @IBOutlet weak var loginStackViewTop: NSLayoutConstraint!

    
    var viewModel: LoginViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        loginBgView?.layer.cornerRadius = 10;
        loginBgView?.layer.borderWidth = 1;
        loginBgView?.layer.borderColor = UIColor.lightGray.cgColor
        emailTextField?.addBottomBorder()
        pwdTextField?.addBottomBorder()
        loginButton?.layer.cornerRadius = 10;
        viewModel = LoginViewModel()
        pwdTextField?.isSecureTextEntry = true
        
        let bounds = UIScreen.main.bounds
        _ = bounds.size.width
        let height = bounds.size.height
        
        print("height")
        print(height)
        
        if(height<720){
            self.loginStackViewTop.constant -= 82
        }
        if(height<890&&height>720){
            self.loginStackViewTop.constant -= 50
        }
        
        let defaults = UserDefaults.standard
        if((defaults.object(forKey:"username")) != nil){
            //emailTextField?.text = (defaults.object(forKey:"username") as! String);
        }
           if((defaults.object(forKey:"password")) != nil){
           // pwdTextField?.text = (defaults.object(forKey:"password") as! String);
                 }
        
        //let email =
        emailTextField?.text = "testsuperadmin@enmovil.in";
        pwdTextField?.text = "test";
    }
    func showAlert(alertMessage:String,title:String) {
        let alert = UIAlertController(title: title, message: alertMessage,         preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { _ in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func loginBtnAction(_ sender: UIBarButtonItem) {
        
        let defaults = UserDefaults.standard
        defaults.set(emailTextField?.text, forKey: "username")
        defaults.set(pwdTextField?.text, forKey: "password")

        
        emailTextField?.resignFirstResponder()
        pwdTextField?.resignFirstResponder()
        
        
        if(emailTextField?.text?.count == 0 || pwdTextField?.text?.count == 0){
            showAlert(alertMessage: "Please enter valid Email and Passsword", title: "Login")
            return;
        }
        //show loading Indicator
        DispatchQueue.main.async {
            LoadingOverlay.shared.showOverlay(view: UIApplication.shared.keyWindow!)
        }
        let dictionary = ["email": emailTextField?.text, "password": pwdTextField?.text]
        viewModel.fetchLoginData(inputData: dictionary, completion: { (isSuccess) in
          
            
            if isSuccess {
                //remove loading indcator
                DispatchQueue.main.async {
                    LoadingOverlay.shared.hideOverlayView()
                    let dashBordVC: DashBordVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DashBordVC") as! DashBordVC
                    SharedObject.sharedInstance.userRoles = self.viewModel.roles
                   // dashBordVC.roles = self.viewModel.roles
                    self.navigationController?.pushViewController(dashBordVC, animated: true)
                    
                  /*  let dashBordVC: HomeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                   // dashBordVC.roles = self.viewModel.roles
                    self.navigationController?.pushViewController(dashBordVC, animated: true)*/
                    
                   // MenuViewController
                }
                
                
            }else{
                self.showAlert(alertMessage: "Login Failed", title: "Login")

            }
        })
   
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
