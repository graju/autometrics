//
//  CriticalListVC.swift
//  Autometrics
//
//  Created by Gangaraju on 30/05/20.
//  Copyright © 2020 Gangaraju. All rights reserved.
//

import UIKit

class CriticalListVC: UITableViewController {

    fileprivate var criticalGroupList:[CriticalListGroup] = [];
    public var criticalTptList:[CriticalTptList] = []
    var deptCode:String = ""
    var deptName:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        self.title = "Critical Delays"
       // self.tableView.backgroundColor = UIColor(red: 29/255, green: 35/255, blue: 48/255, alpha: 1)
        self.tableView.sectionHeaderHeight = 60
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none

        var transportNames:[String] = []
        for critical in criticalTptList {
            print(critical.transporters_name)
            if !transportNames.contains(critical.transporters_name) {
                transportNames.append(critical.transporters_name)
                
            }
        }
        var criticalGrouptemp:[CriticalListGroup] = []
        for transporters_name in transportNames {
            let criticalListGroup = CriticalListGroup();
            for critical in criticalTptList {
                if transporters_name == critical.transporters_name {
                    criticalListGroup.transporters_name = transporters_name;
                    criticalListGroup.criticalList.append(critical)

                    let max = Int(critical.max)!
                    if max > criticalListGroup.max{
                        criticalListGroup.max = max
                    }
                    let min = Int(critical.min)!
                    print(criticalListGroup.min);
                    if(criticalListGroup.min == -1){
                        criticalListGroup.min = min
                    }else{
                        if min < criticalListGroup.min{
                            criticalListGroup.min = min
                        }
                    }
                    
                    criticalListGroup.len = criticalListGroup.len + Int(critical.len)!
                }
            }
            criticalGrouptemp.append(criticalListGroup);
        }
        
        criticalGroupList = criticalGrouptemp.sorted {
            $0.len > $1.len
        }
                

        self.tableView.reloadData()
        print(transportNames);
        
        
}

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return criticalGroupList.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let criticalGroup = criticalGroupList[section]
        print("listCount")
        print(criticalGroup.criticalList.count);
        print("name")
        print(criticalGroup.transporters_name);
        return criticalGroup.criticalList.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let criticalGroup = criticalGroupList[section]
        
        let headerBgView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width , height: 60))
        
        headerBgView.tag = section;
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.headerBgViewTapped(_:)))
           tapGesture.numberOfTapsRequired = 1
           tapGesture.numberOfTouchesRequired = 1
           headerBgView.addGestureRecognizer(tapGesture)
           headerBgView.isUserInteractionEnabled = true
        

         headerBgView.backgroundColor  = UIColor(red: 46/255, green: 59/255, blue: 74/255, alpha: 1)
        
        
        let headerView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width-0 , height: 60))
        headerView.backgroundColor = UIColor(red: 46/255, green: 59/255, blue: 74/255, alpha: 1)
      

        
        let titleLabel: UILabel = UILabel.init(frame: CGRect(x: 20, y:5, width: headerView.frame.size.width-130, height: 25))
        titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
        titleLabel.textColor = UIColor.white
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.text = criticalGroup.transporters_name
        
        let daysLabel: UILabel = UILabel.init(frame: CGRect(x: 20, y: 26, width: headerView.frame.size.width-130, height: 24))
        daysLabel.font = UIFont.boldSystemFont(ofSize: 16)
        daysLabel.textColor = UIColor.white
        daysLabel.backgroundColor = UIColor.clear
        let min:String = String(criticalGroup.min);
        let max:String = String(criticalGroup.max);
        
        if(max == min){
            daysLabel.text =  max + " Days"

        }else{
            daysLabel.text = min + " - " + max + " Days"

        }
        
        let countLabel: UILabel = UILabel.init(frame: CGRect(x: headerView.frame.size.width-120, y: 10, width: 110, height: 30))
        countLabel.font = UIFont.boldSystemFont(ofSize: 25)
        countLabel.textColor = UIColor.white
        countLabel.textColor = UIColor(red: 255/255, green: 0/255, blue: 106/255, alpha: 1)
        countLabel.backgroundColor = UIColor.clear
        countLabel.textAlignment = NSTextAlignment.right
        countLabel.text = String(criticalGroup.len)
        
        
        headerView.addSubview(titleLabel)
        headerView.addSubview(daysLabel)
        headerView.addSubview(countLabel)
        headerBgView.addSubview(headerView)
        return headerBgView
        
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You selected cell #\(indexPath.row)!")
        let criticalGroup = criticalGroupList[indexPath.section]
        let critical = criticalGroup.criticalList[indexPath.row]
        
        
        let deptWiseDataVC: DeptWiseDataVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DeptWiseDataVC") as! DeptWiseDataVC
        deptWiseDataVC.transporterName = criticalGroup.transporters_name
        deptWiseDataVC.title = critical.consigner_code
        deptWiseDataVC.deptCode = self.deptCode;
        deptWiseDataVC.deptName = self.deptName;
                          self.navigationController?.pushViewController(deptWiseDataVC, animated: true)
    }
 
    
    @objc func headerBgViewTapped(_ gestureRecognizer: UITapGestureRecognizer) {
        let tag = gestureRecognizer.view?.tag
        let criticalGroup = criticalGroupList[tag!]
        let deptWiseDataVC: DeptWiseDataVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DeptWiseDataVC") as! DeptWiseDataVC
        deptWiseDataVC.title = criticalGroup.transporters_name
        deptWiseDataVC.transporterName = criticalGroup.transporters_name
        deptWiseDataVC.deptCode = self.deptCode;
        deptWiseDataVC.deptName = self.deptName;
        self.navigationController?.pushViewController(deptWiseDataVC, animated: true)

        print("Hello");
        print(tag as Any)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        //self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        var cellBgview  = cell.contentView.viewWithTag(1);
        
        if(cellBgview == nil){
            cellBgview = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width-0, height: 44))
            //view.backgroundColor = UIColor(red: 46/255, green: 59/255, blue: 74/255, alpha: 1)
            cell.contentView.addSubview(cellBgview!);
            cellBgview!.tag = 1;
        }
        
        var titleLabel:UILabel?  = cellBgview!.viewWithTag(2) as? UILabel;
        if(titleLabel == nil){
            titleLabel = UILabel(frame: CGRect(x: 20, y: 0, width: cellBgview!.frame.size.width-100, height: 44))
            titleLabel!.tag = 2;
            titleLabel!.font = UIFont.systemFont(ofSize: 18)
            titleLabel!.backgroundColor = UIColor.clear
            titleLabel!.textColor = UIColor.black
            cellBgview!.addSubview(titleLabel!);

        }

        var countLabel:UILabel?  = cellBgview!.viewWithTag(3) as? UILabel;

        if(countLabel == nil){
            countLabel = UILabel(frame: CGRect(x: cellBgview!.frame.size.width-100, y: 0, width: 85, height: 44))
            
            countLabel!.tag = 3;
            
            
            countLabel!.font = UIFont.systemFont(ofSize: 16)
            countLabel!.textAlignment = NSTextAlignment.right
            countLabel!.textColor = UIColor.black

            countLabel!.backgroundColor = UIColor.clear
            cellBgview!.addSubview(countLabel!);
            
        }
       
        
        
        
        /*if(indexPath.row%2 == 0){
            cell.backgroundColor  = UIColor(red: 46/255, green: 55/255, blue: 71/255, alpha: 1)
        }else{
            cell.backgroundColor  = UIColor(red: 46/255, green: 59/255, blue: 74/255, alpha: 1)
        }*/
        
        cell.backgroundColor = UIColor.white
        
        
        
        
        let criticalGroup = criticalGroupList[indexPath.section]
        let critical = criticalGroup.criticalList[indexPath.row]
        
        
        titleLabel!.text = critical.consigner_code
        countLabel!.text = critical.len
        
      //  cell.textLabel?.text = critical.consigner_code + "  " + critical.len
            // cell.textLabel?.textColor = UIColor.white
        // Configure the cell...

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
