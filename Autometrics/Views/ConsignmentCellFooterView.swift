//
//  ConsignmentCellFooterView.swift
//  Autometrics
//
//  Created by Khaja Bee on 06/06/20.
//  Copyright © 2020 Gangaraju. All rights reserved.
//

import UIKit

class ConsignmentCellFooterView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var footerStackView: UIStackView!
    @IBOutlet weak var footerBgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
