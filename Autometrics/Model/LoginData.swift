//
//  LoginData.swift
//  Autometrics
//
//  Created by Gangaraju on 28/05/20.
//  Copyright © 2020 Gangaraju. All rights reserved.
//

import UIKit

struct User:Codable{
    let roles:[String]
}
struct LoginData: Codable {
    let auth: Bool
    let token:String
    let msg:String
    let user:User;
}
