//
//  LoadingOverlay.swift
//  SptechAssignment
//
//  Created by ganga raju on 15/09/19.
//  Copyright © 2019 Sample. All rights reserved.
//

import UIKit

public class LoadingOverlay{
    
    var bgView = UIView()
    var loadingImgView = UIImageView(image: UIImage.gifImageWithName("loader"))
    
    class var shared: LoadingOverlay {
        struct Static {
            static let instance: LoadingOverlay = LoadingOverlay()
        }
        return Static.instance
    }
    
    public func showOverlay(view: UIView) {
        bgView.frame = view.frame
        bgView.backgroundColor = UIColor.gray
        bgView.autoresizingMask = [.flexibleLeftMargin,.flexibleTopMargin,.flexibleRightMargin,.flexibleBottomMargin,.flexibleHeight, .flexibleWidth]
        bgView.alpha = 0.5;
        loadingImgView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        loadingImgView.center = bgView.center
        view.addSubview(bgView)
        view.addSubview(loadingImgView)
    }
    
    public func hideOverlayView() {
        loadingImgView.removeFromSuperview()
        bgView.removeFromSuperview()
    }
}

