//
//  ConsignmentDataCell.swift
//  Autometrics
//
//  Created by Khaja Bee on 06/06/20.
//  Copyright © 2020 Gangaraju. All rights reserved.
//

import UIKit

class ConsignmentDataCell: UITableViewCell {

    @IBOutlet weak var consignNameHeaderLbl: UILabel?
    @IBOutlet weak var consigneeNameLbl: UILabel?
    @IBOutlet weak var destinHeaderCityLbl: UILabel?
    @IBOutlet weak var destinationCityLbl: UILabel?
    @IBOutlet weak var originalEtaHeaderLbl: UILabel?
    @IBOutlet weak var originalEtaLbl: UILabel?
    @IBOutlet weak var transitDelayHeaderLbl: UILabel?
    @IBOutlet weak var transitDelayLbl: UILabel?
    @IBOutlet weak var spocHeaderLbl: UILabel?
    @IBOutlet weak var spocLbl: UILabel?
    @IBOutlet weak var spocNameLbl: UILabel?
    @IBOutlet weak var phoneCallBtn: UIButton?
    @IBOutlet weak var remarksHeaderLbl: UILabel?
    @IBOutlet weak var remarksLbl: UILabel?
    @IBOutlet weak var currentCityHeaderLbl: UILabel!
    @IBOutlet weak var currentCityNameLbl: UILabel!
    @IBOutlet weak var gpsLocationHeaderLbl: UILabel!
    
    @IBOutlet weak var gpsLocationLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        consignNameHeaderLbl?.text = "Consignee Name"
        consignNameHeaderLbl?.font = UIFont.systemFont(ofSize: 12)
        consigneeNameLbl?.font = UIFont.systemFont(ofSize: 15)
        consigneeNameLbl!.lineBreakMode = .byTruncatingTail

        
        destinHeaderCityLbl?.text = "Destination City"
        destinHeaderCityLbl?.font = UIFont.systemFont(ofSize: 12)
        destinationCityLbl?.font = UIFont.systemFont(ofSize: 15)

        
        originalEtaHeaderLbl?.text = "Original ETA"
        originalEtaHeaderLbl?.font = UIFont.systemFont(ofSize: 12)
        originalEtaLbl?.font = UIFont.systemFont(ofSize: 15)
        
        
        transitDelayHeaderLbl?.text = "Transit Delay(Days)"
        transitDelayHeaderLbl?.font = UIFont.systemFont(ofSize: 12)
        transitDelayLbl?.font = UIFont.systemFont(ofSize: 15)
        
        spocHeaderLbl?.text = "Transpoter Name"
        spocHeaderLbl?.font = UIFont.systemFont(ofSize: 12)
        spocLbl?.font = UIFont.systemFont(ofSize: 15)

        spocNameLbl?.text = "Transpoter Name"
        spocNameLbl?.font = UIFont.systemFont(ofSize: 12)
        
        currentCityHeaderLbl?.text = "Current City/State"
        currentCityHeaderLbl?.font = UIFont.systemFont(ofSize: 12)
        currentCityNameLbl?.font = UIFont.systemFont(ofSize: 15)
        
        gpsLocationHeaderLbl?.text = "Last GPS Location"
        gpsLocationHeaderLbl?.font = UIFont.systemFont(ofSize: 12)
        gpsLocationLbl?.font = UIFont.systemFont(ofSize: 15)
        
        remarksHeaderLbl?.text = "Remarks"
        remarksHeaderLbl?.font = UIFont.systemFont(ofSize: 12)
               
        remarksLbl?.font = UIFont.systemFont(ofSize: 15)
        
        //emptyLabl?.text = "";
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
