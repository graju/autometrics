//
//  DeptWiseDataViewModel.swift
//  Autometrics
//
//  Created by Entrac Solutions on 01/06/20.
//  Copyright © 2020 Gangaraju. All rights reserved.
//

import UIKit


struct DeptWiseDataRequest: RequestType{
    var data: RequestData
    typealias ResponseType = DeptmentWiseData
}

class DeptWiseDataViewModel: NSObject {
    public var deptWiseDatataArray: [CriticalData] = []
    typealias DeptWiseDataCompletion = ((_ isSuccess: Bool) -> Void)
    
      func fetchDeptWiseData(inputData:[String: Any?], completion: @escaping DeptWiseDataCompletion){
          
        let deptWiseDataRequest = DeptWiseDataRequest(data: RequestData(method: .post, url: url.deptWiseChart.rawValue,bodyParmaneters:inputData,requestbodyType:.parmerters))
        
        
          
          deptWiseDataRequest.serviceCall(
              onSuccess: { (deptWiseResponseData: DeptmentWiseData) in
                if(deptWiseResponseData.message.count>0){
                    
                    self.deptWiseDatataArray = deptWiseResponseData.message[0].critical_data;
                }
                

                  completion(true)
          },
              onError: { (error: Error) in
                  completion(false)
                  DispatchQueue.main.async {
                      LoadingOverlay.shared.hideOverlayView()
                      
                  }
                  switch error.localizedDescription {
                  case ErrorMessages.noConnection.rawValue:
                      print("Error");
                  //self.fetchOfflineData(completion: completion)
                  default:
                      print(error.localizedDescription)
                  }
                  
                  
          })
      }
}

