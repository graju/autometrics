//
//  DeptWiseDataCell.swift
//  Autometrics
//
//  Created by Entrac Solutions on 02/06/20.
//  Copyright © 2020 Gangaraju. All rights reserved.
//

import UIKit



@IBDesignable
/*class RightAlignedIconButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        semanticContentAttribute = .forceRightToLeft
        contentHorizontalAlignment = .right
        let availableSpace = bounds.inset(by: contentEdgeInsets)
        let availableWidth = availableSpace.width - imageEdgeInsets.left - (imageView?.frame.width ?? 0) - (titleLabel?.frame.width ?? 0)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: availableWidth / 2)
    }
}*/
class DeptWiseDataCell: UITableViewCell {

    @IBOutlet weak var consignNameHeaderLbl: UILabel?
    @IBOutlet weak var consigneeNameLbl: UILabel?
    @IBOutlet weak var destinHeaderCityLbl: UILabel?
    @IBOutlet weak var destinationCityLbl: UILabel?
    @IBOutlet weak var originalEtaHeaderLbl: UILabel?
    @IBOutlet weak var originalEtaLbl: UILabel?
    @IBOutlet weak var transitDelayHeaderLbl: UILabel?
    @IBOutlet weak var transitDelayLbl: UILabel?
    @IBOutlet weak var spocHeaderLbl: UILabel?
    @IBOutlet weak var spocLbl: UILabel?
    @IBOutlet weak var emptyLabl: UILabel?
    @IBOutlet weak var phoneCallBtn: UIButton?
    @IBOutlet weak var remarksHeaderLbl: UILabel?
    @IBOutlet weak var remarksLbl: UILabel?

   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        consignNameHeaderLbl?.text = "Consignee Name"
        consignNameHeaderLbl?.font = UIFont.systemFont(ofSize: 12)
        consigneeNameLbl?.font = UIFont.systemFont(ofSize: 15)
        consigneeNameLbl!.lineBreakMode = .byTruncatingTail

        
        destinHeaderCityLbl?.text = "Destination City"
        destinHeaderCityLbl?.font = UIFont.systemFont(ofSize: 12)
        destinationCityLbl?.font = UIFont.systemFont(ofSize: 15)

        
        originalEtaHeaderLbl?.text = "Original ETA"
        originalEtaHeaderLbl?.font = UIFont.systemFont(ofSize: 12)
        originalEtaLbl?.font = UIFont.systemFont(ofSize: 15)
        
        
        transitDelayHeaderLbl?.text = "Transit Delay(Days)"
        transitDelayHeaderLbl?.font = UIFont.systemFont(ofSize: 12)
        transitDelayLbl?.font = UIFont.systemFont(ofSize: 15)
        
        spocHeaderLbl?.text = "SPOC"
        spocHeaderLbl?.font = UIFont.systemFont(ofSize: 12)
        spocLbl?.font = UIFont.systemFont(ofSize: 15)

        

        
        remarksHeaderLbl?.text = "Remarks"
        remarksHeaderLbl?.font = UIFont.systemFont(ofSize: 12)
               
        remarksLbl?.font = UIFont.systemFont(ofSize: 15)
        
        //emptyLabl?.text = "";
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
