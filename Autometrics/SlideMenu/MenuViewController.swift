//
//  MenuViewController.swift
//  AKSwiftSlideMenu
//
//  Created by Ashish on 21/09/15.
//  Copyright (c) 2015 Kode. All rights reserved.
//

import UIKit


protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
}

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    let kHeaderSectionTag: Int = 6900;
    var expandedSectionHeaderNumber: Int = -1
    
    var  menuData =  [["title":"Dashboard","rows":[]],["title":"Sales and Dispatch","rows":["Active Consignments","Trasshipments"]],["title":"Spare Parts (Domestic)","rows":["Active Consignments","Trasshipments"]],["title":"Spare Parts (Courier)","rows":["Active Consignments"]],["title":"Spare Parts (Exports)","rows":["Active Consignments"]],["title":"Supply Chain (Container Imports)","rows":["Active Consignments"]],["title":"Supply Chain (Container Exports)","rows":["Active Consignments"]],["title":"Logout","rows":[]]]
    /**
    *  Array to display menu options
    */
    @IBOutlet var tblMenuOptions : UITableView!
    
    /**
    *  Transparent button to hide menu
    */
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    
    /**
    *  Array containing menu options
    */
   // var arrayMenuOptions = [Dictionary<String,String>]()
    
    /**
    *  Menu button which was tapped to display the menu
    */
    var btnMenu : UIButton!
    
    /**
    *  Delegate of the MenuVC
    */
    var delegate : SlideMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblMenuOptions.tableFooterView = UIView()
        self.tblMenuOptions.backgroundColor = UIColor(red: 29/255, green: 35/255, blue: 48/255, alpha: 1)
        self.tblMenuOptions.sectionHeaderHeight = 54
        
       /* sectionNames = [ "iPhone", "iPad", "Apple Watch" ];
               sectionItems = [ ["iPhone 5", "iPhone 5s", "iPhone 6", "iPhone 6 Plus", "iPhone 7", "iPhone 7 Plus"],
                                ["iPad Mini", "iPad Air 2", "iPad Pro", "iPad Pro 9.7"],
                                ["Apple Watch", "Apple Watch 2", "Apple Watch 2 (Nike)"]
                              ];*/
       
      

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // updateArrayMenuOptions()
    }
    
    func updateArrayMenuOptions(){
       // arrayMenuOptions.append(["title":"Dashboard", "icon":"dashboard"])
        //arrayMenuOptions.append(["title":"Logout", "icon":"logout"])
        
        //tblMenuOptions.reloadData()
    }
   
    
    
    @IBAction func onCloseMenuClick(_ button:UIButton!){
        btnMenu.tag = 0
        
        if (self.delegate != nil) {
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                self.view.removeFromSuperview()
                self.removeFromParent()
        })
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54.0
      }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellMenu")!
        
       
        cell.accessoryType = .disclosureIndicator
        let section = self.menuData[indexPath.section]
        cell.textLabel?.textColor = UIColor.black

        let rows:[String] = section["rows"] as! [String]
        cell.textLabel?.text = rows[indexPath.row]
               
        
        return cell
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
         if (self.menuData.count != 0) {
                   return self.menuData[section]["title"] as? String
               }
               return ""
     }
     
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
         return 54.0;
     }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //recast your view as a UITableViewHeaderFooterView
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor(red: 29/255, green: 35/255, blue: 48/255, alpha: 1)
        header.textLabel?.textColor = UIColor.white
        
        if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section) {
            viewWithTag.removeFromSuperview()
        }
        let sectionTitle:String = self.menuData[section]["title"] as! String

        
        let headerFrame = self.view.frame.size
        let theImageView = UIImageView(frame: CGRect(x: headerFrame.width - 86, y: 13, width: 18, height: 18));
        if(sectionTitle == "Logout" || sectionTitle == "Dashboard"){
            theImageView.image = UIImage(named: "rightArrow")
        }else{
            theImageView.image = UIImage(named: "Chevron-Dn-Wht")
        }
        theImageView.tag = kHeaderSectionTag + section
        header.addSubview(theImageView)
        
        let borderLineview = UIView(frame: CGRect(x: 0, y: header.frame.size.height-1, width: header.frame.size.width, height: 1))
        borderLineview.backgroundColor = UIColor.white
        header.addSubview(borderLineview)

        
        // make headers touchable
        header.tag = section
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.headerViewTapped(_:)))
        header.addGestureRecognizer(tapGesture)
    }
     @objc func headerViewTapped(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! UITableViewHeaderFooterView
            let section    = headerView.tag
        let sectionTitle:String = self.menuData[section]["title"] as! String
        if(sectionTitle == "Logout"){
            self.navigationController?.popToRootViewController(animated: true)
        }else if(sectionTitle == "Dashboard"){
            let dashBordVC: DashBordVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DashBordVC") as! DashBordVC
                             //  dashBordVC.roles = self.viewModel.roles
                               self.navigationController?.pushViewController(dashBordVC, animated: true)
        }else{
            let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as? UIImageView
                         
                         if (self.expandedSectionHeaderNumber == -1) {
                             self.expandedSectionHeaderNumber = section
                             tableViewExpandSection(section, imageView: eImageView!)
                         } else {
                             if (self.expandedSectionHeaderNumber == section) {
                                 tableViewCollapeSection(section, imageView: eImageView!)
                             } else {
                                 let cImageView = self.view.viewWithTag(kHeaderSectionTag + self.expandedSectionHeaderNumber) as? UIImageView
                                 tableViewCollapeSection(self.expandedSectionHeaderNumber, imageView: cImageView!)
                                 tableViewExpandSection(section, imageView: eImageView!)
                             }
                         }
        }
        
             
    }
    func tableViewCollapeSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.menuData[section]
        
        self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0) {
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
         
            let rows:[String] = sectionData["rows"] as! [String]
            for i in 0 ..< rows.count {
                let index = IndexPath(row: i, section: section)
                    indexesPath.append(index)
                }
            self.tblMenuOptions!.beginUpdates()
            self.tblMenuOptions!.deleteRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tblMenuOptions!.endUpdates()
        }
    }
    
    func tableViewExpandSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.menuData[section]
        
        if (sectionData.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
        
            let rows:[String] = sectionData["rows"] as! [String]

            for i in 0 ..< rows.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.expandedSectionHeaderNumber = section
            self.tblMenuOptions!.beginUpdates()
            self.tblMenuOptions!.insertRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tblMenuOptions!.endUpdates()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let btn = UIButton(type: UIButton.ButtonType.custom)
        btn.tag = indexPath.row
        let section = self.menuData[indexPath.section]

        let rows:[String] = section["rows"] as! [String]
               
        if(rows[indexPath.row] == "Logout"){
            self.navigationController?.popToRootViewController(animated: true)
        }else if(rows[indexPath.row] == "Active Consignments"){
            let sectionTitle:String = section["title"] as! String
            var deptCode = "";
            if(sectionTitle == "Sales and Dispatch"){
                deptCode = "SDNG"
            }else {
                
            }
            let consignmentDataVC: ConsignmentDataVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ConsignmentDataVC") as! ConsignmentDataVC
            consignmentDataVC.deptCode = deptCode
            self.navigationController?.pushViewController(consignmentDataVC, animated: true)
            self.onCloseMenuClick(btn) 
        }
    }
                   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       if (self.expandedSectionHeaderNumber == section) {
        let menuObject:Array = self.menuData[section]["rows"] as! Array<String>
        return menuObject.count
               } else {
                   return 0;
               }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
         if menuData.count > 0 {
                   tableView.backgroundView = nil
                   return menuData.count
               } else {
                   let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height))
                   messageLabel.text = "Retrieving data.\nPlease wait."
                   messageLabel.numberOfLines = 0;
                   messageLabel.textAlignment = .center;
                   messageLabel.font = UIFont(name: "HelveticaNeue", size: 20.0)!
                   messageLabel.sizeToFit()
                   self.tblMenuOptions.backgroundView = messageLabel;
               }
               return 0
    }
   
   
}
