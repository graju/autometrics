//
//  NetWorkManager.swift
//  FXHelper
//
//  Created by ganga raju on 07/09/19.
//  Copyright © 2019 Sample. All rights reserved.
//




import UIKit

public enum url: String{
    case login = "http://autometrics.in/foxtrot/mobilelogin"
    case dashboard = "http://164.52.199.6/deptWiseActiveConsignmentDashboardData"
    case deptWiseChart = "http://164.52.199.6/deptWiseChartConsignmentDashboardData"
    case consignments = "http://164.52.199.6/getActiveConsignmentsForMobile"
}

public enum RequestBodyType: String{
    case json = "json"
    case parmerters = "parameters"
}

public enum HTTPMethod: String{
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
    
}
enum NetworkEnvironment{
    case qa
    case production
    case staging
}
public enum ConnError: Swift.Error {
    case invalidURL
    case noData
    case notReachble
}
public enum ErrorMessages: String {
    case noConnection = "The Internet connection appears to be offline."
}

public struct RequestData{
    let url:String;
    let httpMethod:HTTPMethod;
    var bodyParms:[String: Any?]?
    let headers:[String:String]?
    let requestBodyType:RequestBodyType;

    
    
    init( method: HTTPMethod = .get,
          url: String,
          bodyParmaneters: [String: Any?]? = nil,
          headers: [String:String]? = nil,requestbodyType:RequestBodyType = .json) {
        
        self.httpMethod = method
        self.bodyParms = bodyParmaneters
        self.headers = headers
        self.requestBodyType = requestbodyType;
        self.url = url;
    }
    
    
    
}
public protocol RequestType{
    associatedtype ResponseType: Codable
    var data: RequestData { get set }
}
public extension RequestType {
    func serviceCall(onSuccess: @escaping (ResponseType) -> Void,
                      onError:@escaping (Error) -> Void) {
        //self.data.bodyParms = inputData

        let dispatcher: NetworkDispatcher = URLSessionNetworkDispatcher.instance
        dispatcher.dispatch(request: self.data,  onSuccess: { (responseData: Data) in
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode(ResponseType.self, from: responseData)
                DispatchQueue.main.async {
                    onSuccess(result)
                    
                }
            } catch let error {
                DispatchQueue.main.async {
                    print("Decode  Error")
                    onError(error)
                }
            }
        },
        onError: { (error: Error) in
                DispatchQueue.main.async {
                    onError(error)
                }
            }
        )
        
    }
}

public protocol NetworkDispatcher {
    func dispatch(request: RequestData, onSuccess: @escaping (Data) -> Void, onError: @escaping (Error) -> Void)
}

public struct URLSessionNetworkDispatcher: NetworkDispatcher {
    public static let instance = URLSessionNetworkDispatcher()
    private init() {}
    
    public func dispatch(request: RequestData, onSuccess: @escaping (Data) -> Void, onError: @escaping (Error) -> Void) {
    
        guard let url = URL(string: request.url) else {
            onError(ConnError.invalidURL)
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = request.httpMethod.rawValue
        
        if(request.requestBodyType == .parmerters){
            var paramString = ""
            
            if let params = request.bodyParms {
                for dict in params{
                    paramString = paramString + dict.key + "=" + (dict.value as! String)
                }
                urlRequest.httpBody = paramString.data(using: String.Encoding.utf8)
            }
            
        }else{
            
                   urlRequest.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")  // the request is JSON
                   urlRequest.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")        // the expected response is also JSON
            do {
                                  if let params = request.bodyParms {
                                      print(params);
                                      urlRequest.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
                                  }
                              } catch let error {
                                  onError(error)
                                  return
                              }
            if let headers = request.headers {
                               urlRequest.allHTTPHeaderFields = headers
                           }
        }
 
                   URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
                       if let error = error {
                         
                          onError(error)
                           
                           return
                       }
                       guard let _data = data else {
                           onError(ConnError.noData)
                           return
                       }
                       let str = String(decoding: data!, as: UTF8.self)
                       print(str)

                       onSuccess(_data)
                       }.resume()
        
        
       
    }
}
