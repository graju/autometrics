//
//  DeptWiseDataVC.swift
//  Autometrics
//
//  Created by Entrac Solutions on 02/06/20.
//  Copyright © 2020 Gangaraju. All rights reserved.
//

import UIKit

// as UISearchBar extension
// as UISearchBar extension

extension DeptWiseDataVC:UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController) {
   
    
        let searchText:String = searchController.searchBar.text!;
       
      //print(self.viewModel.deptWiseDatataArray)
        if(searchText.count>0){
               self.filteredArray = self.viewModel.deptWiseDatataArray.filter { $0.consignee_name.localizedCaseInsensitiveContains(searchText) || $0.consignee_city.localizedCaseInsensitiveContains(searchText) || $0.spoc_contact_name.localizedCaseInsensitiveContains(searchText) || $0.expected_trip_end.localizedCaseInsensitiveContains(searchText) || $0.transit_delay_days.localizedCaseInsensitiveContains(searchText) || $0.consignment_code.localizedCaseInsensitiveContains(searchText) || $0.plant_location.localizedCaseInsensitiveContains(searchText)  }
         
        }else{
            self.filterDaata()
        }
        print(self.filteredArray);
        self.tableView.reloadData()
    
    }
       
}

class DeptWiseDataVC: UITableViewController, UISearchBarDelegate {

    

    var viewModel: DeptWiseDataViewModel!
    var filteredArray:[CriticalData] = []
    var transporterName:String = ""
    var deptCode:String = ""
    var deptName:String = ""
    var resultSearchController = UISearchController()

    @IBAction func phoneBtnAction(_ sender: UIButton) {
        let bttnTag = sender.tag
        let  criticalData = self.viewModel.deptWiseDatataArray[bttnTag]
        
        print(bttnTag)
        
        
       // let phoneNumber:String = (sender.titleLabel?.text!)!
        if let url = URL(string: "tel://\(criticalData.spoc_contact_number)"),
        UIApplication.shared.canOpenURL(url) {
           if #available(iOS 10, *) {
             UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            print("Dont have this future in this device")
                 // add error message here
        }
    }
    func showAlert(alertMessage:String,title:String) {
        let alert = UIAlertController(title: title, message: alertMessage,         preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { _ in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.sectionHeaderHeight = 60

        viewModel = DeptWiseDataViewModel()


       resultSearchController = ({
            let searchController = UISearchController(searchResultsController: nil)
            searchController.searchResultsUpdater = self
            searchController.dimsBackgroundDuringPresentation = false
            searchController.searchBar.sizeToFit()
            searchController.searchBar.backgroundColor = UIColor.white;
        
        
        //searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true

        
       // self.tableView.tableHeaderView = controller.searchBar
         // Place the search bar in the navigation bar.
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            searchController.searchBar.autocapitalizationType = .none
            searchController.dimsBackgroundDuringPresentation = false
            searchController.searchBar.delegate = self // Monitor when the search button is tapped.
            searchController.searchBar.backgroundColor = UIColor.white
            self.navigationItem.searchController = searchController


            
            // Make the search bar always visible.
            navigationItem.hidesSearchBarWhenScrolling = false
          
            definesPresentationContext = true
            
        } else {
            // Fallback on earlier versions
        }
      
           
        return searchController
        })()
        
        if #available(iOS 11.0, *) {
           // resultSearchController.searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor.gray]
       UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        
        self.tableView.reloadData()
        
        //show loading Indicator
         DispatchQueue.main.async {
            LoadingOverlay.shared.showOverlay(view: UIApplication.shared.keyWindow!)
        }
        let dictionary = [ "dept_code": self.deptCode]
       viewModel.fetchDeptWiseData(inputData: dictionary, completion: { (isSuccess) in
          
            
            if isSuccess {
                //remove loading indcator
                DispatchQueue.main.async {
                    LoadingOverlay.shared.hideOverlayView()
                    self.filterDaata()
                   
                    print("Count....")
                    print(self.viewModel.deptWiseDatataArray.count)
                    

                    self.tableView.reloadData()
                }
                
                
            }else{
                self.showAlert(alertMessage: "DeptWiseData Failed", title: "")

            }
        })
    }

    func filterDaata(){
        //transport name wise search
        if(self.transporterName == self.title){
            if  (!resultSearchController.isActive) {
                self.viewModel.deptWiseDatataArray = self.viewModel.deptWiseDatataArray.filter { $0.transporter_name == self.transporterName }
                           
            }
           
            self.filteredArray = self.viewModel.deptWiseDatataArray.filter { $0.transporter_name == self.title
                
            }
        }else{
            //transport name and consigner_code wise search
            if  (!resultSearchController.isActive) {
                
                 self.viewModel.deptWiseDatataArray = self.viewModel.deptWiseDatataArray.filter { $0.consigner_code == self.title && $0.transporter_name == self.transporterName  }
                
            }
             self.filteredArray = self.viewModel.deptWiseDatataArray.filter {
                $0.consigner_code == self.title && $0.transporter_name == self.transporterName
            }
           
        }
       /* self.viewModel.deptWiseDatataArray = self.viewModel.deptWiseDatataArray.sorted {
            $0.transit_delay_days.localizedCaseInsensitiveCompare($1.transit_delay_days) == ComparisonResult.orderedDescending
        }*/
       
        self.viewModel.deptWiseDatataArray = self.viewModel.deptWiseDatataArray.sorted {
            $0.transit_delay_days.localizedStandardCompare($1.transit_delay_days) == ComparisonResult.orderedDescending
        }
        self.filteredArray = self.filteredArray.sorted {
        $0.transit_delay_days.localizedStandardCompare($1.transit_delay_days) == ComparisonResult.orderedDescending
        }
        
        //Decending oreder
  /*  self.viewModel.deptWiseDatataArray = self.viewModel.deptWiseDatataArray.sorted {
                     $0.transit_delay_days > $1.transit_delay_days
                 }
    self.filteredArray = self.filteredArray.sorted {
                   $0.transit_delay_days > $1.transit_delay_days
        }*/
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        
        if  (resultSearchController.isActive) {
            return self.filteredArray.count
        } else {
            return self.viewModel.deptWiseDatataArray.count
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
   
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 240
       }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {


        var  criticalData = self.viewModel.deptWiseDatataArray[section]
        if  (resultSearchController.isActive) {
            criticalData = self.filteredArray[section]
        }

        let headerBgView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width , height: 60))
         headerBgView.backgroundColor  = UIColor(red: 46/255, green: 59/255, blue: 74/255, alpha: 1)
        
        
        let headerView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width-0 , height: 60))
        headerView.backgroundColor = UIColor(red: 46/255, green: 59/255, blue: 74/255, alpha: 1)
      

        
        let firstColumnWidth = (headerView.frame.size.width/2)-25
        
        let truckNoHeaderLbl: UILabel = UILabel.init(frame: CGRect(x: 8, y:5, width: firstColumnWidth, height: 24))
        truckNoHeaderLbl.font = UIFont.systemFont(ofSize: 13)
        truckNoHeaderLbl.textColor = UIColor.white
        truckNoHeaderLbl.backgroundColor = UIColor.clear
        if(self.deptName == "Spare Parts (Courier)"){
            truckNoHeaderLbl.text = "Consiignments No"

        }else{
            truckNoHeaderLbl.text = "Truck No"

        }
        
        
        let truckNoLbl: UILabel = UILabel.init(frame: CGRect(x: 8, y: 30, width: firstColumnWidth, height: 25))
        truckNoLbl.font = UIFont.systemFont(ofSize: 16)
        truckNoLbl.textColor = UIColor.white
        truckNoLbl.backgroundColor = UIColor.clear
        truckNoLbl.text = criticalData.consignment_code
        
        let secondtColunWidth = (headerView.frame.size.width/2)-8
        
        let loadingPlanetHeaderLbl: UILabel = UILabel.init(frame: CGRect(x: truckNoHeaderLbl.frame.size.width+25, y: 5, width: secondtColunWidth, height: 24))
        loadingPlanetHeaderLbl.font = UIFont.systemFont(ofSize: 13)
        loadingPlanetHeaderLbl.textColor = UIColor.white
        loadingPlanetHeaderLbl.backgroundColor = UIColor.clear
        loadingPlanetHeaderLbl.textAlignment = NSTextAlignment.right
        loadingPlanetHeaderLbl.text = "Loading Plant"
        
        
        let loadingPlanetLbl: UILabel = UILabel.init(frame: CGRect(x: truckNoHeaderLbl.frame.size.width+25, y: 30, width: secondtColunWidth, height: 25))
        loadingPlanetLbl.font = UIFont.systemFont(ofSize: 16)
        loadingPlanetLbl.textColor = UIColor.white
        loadingPlanetLbl.backgroundColor = UIColor.clear
        loadingPlanetLbl.textAlignment = NSTextAlignment.right
        
        
        loadingPlanetLbl.text = criticalData.plant_location

        
        headerView.addSubview(truckNoHeaderLbl)
        headerView.addSubview(truckNoLbl)
        headerView.addSubview(loadingPlanetHeaderLbl)
        headerView.addSubview(loadingPlanetLbl)
        headerBgView.addSubview(headerView)
        return headerBgView
        
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! DeptWiseDataCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none

        var  criticalData = self.viewModel.deptWiseDatataArray[indexPath.section]
        if  (resultSearchController.isActive) {
            criticalData = self.filteredArray[indexPath.section]
        }
        
        cell.consigneeNameLbl?.text = criticalData.consignee_name
        cell.destinationCityLbl?.text = criticalData.consignee_city


        cell.transitDelayLbl?.text = criticalData.transit_delay_days
        cell.originalEtaLbl?.text = criticalData.expected_trip_end
        cell.originalEtaLbl?.text = self.formattedDateFromString(dateStr: criticalData.expected_trip_end)
        cell.phoneCallBtn?.setTitle(criticalData.spoc_contact_number, for: .normal)
        cell.spocLbl?.text = criticalData.spoc_contact_name
        if(criticalData.spoc_contact_number.count>0){
            cell.phoneCallBtn?.isHidden = false;
        }else{
            cell.phoneCallBtn?.isHidden = true;
        }
       // cell.phoneCallBtn?.tag = indexPath.section
        cell.remarksLbl?.text = criticalData.delay_reason
        
        return cell
    }
    

    func formattedDateFromString(dateStr:String) -> String{
        let dateFormatter = DateFormatter();
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let convertedDate = dateFormatter.date(from: dateStr){
            
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = "dd/MM/yyyy";
            return outputFormatter.string(from: convertedDate)
            
        }
        return "";
        
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
  
}
