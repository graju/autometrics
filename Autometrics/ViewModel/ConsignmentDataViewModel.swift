//
//  ConsignmentDataViewModel.swift
//  Autometrics
//
//  Created by Khaja Bee on 06/06/20.
//  Copyright © 2020 Gangaraju. All rights reserved.
//

import UIKit

struct ConsignmentModelRequest: RequestType {
    var data: RequestData
    typealias ResponseType = ConsignmentsData
}

class ConsignmentDataViewModel: NSObject {
    public var consignmentDatataArray: [Consignments] = []
    typealias ConsignmentDataCompletion = ((_ isSuccess: Bool) -> Void)
    
      func fetchConsignmentData(inputData:[String: Any?], completion: @escaping ConsignmentDataCompletion){
          
        let consignmentModelRequest = ConsignmentModelRequest(data: RequestData(method: .post, url: url.consignments.rawValue,bodyParmaneters:inputData,requestbodyType:.parmerters))
        
        
          
          consignmentModelRequest.serviceCall(
            onSuccess: {(consignmentResponseData: ConsignmentsData) in
                if(consignmentResponseData.consignments.count>0){
                    
                    self.consignmentDatataArray = consignmentResponseData.consignments;
                }
                

                  completion(true)
          },
              onError: { (error: Error) in
                  completion(false)
                  DispatchQueue.main.async {
                      LoadingOverlay.shared.hideOverlayView()
                      
                  }
                  switch error.localizedDescription {
                  case ErrorMessages.noConnection.rawValue:
                      print("Error");
                  //self.fetchOfflineData(completion: completion)
                  default:
                      print(error.localizedDescription)
                  }
                  
                  
          })
      }
}
