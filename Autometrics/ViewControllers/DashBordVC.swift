//
//  DashBordVC.swift
//  Autometrics
//
//  Created by Gangaraju on 28/05/20.
//  Copyright © 2020 Gangaraju. All rights reserved.
//

import UIKit
import Highcharts

extension DashBordVC:UITableViewDelegate,UITableViewDataSource{
    // MARK: - Table view data source
    
     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.viewModel.dashBordDataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 310.0
    }
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cellReuseIdentifier: String = "cellReuseIdentifier"
        
        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier)
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellReuseIdentifier)
            
            cell!.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let chartView = HIChartView(frame: CGRect(x: 5.0, y: 5.0, width: self.view.bounds.size.width - 10, height: 290.0))
          //  chartView.backgroundColor = UIColor(red: 29/255, green: 35/255, blue: 48/255, alpha: 1)
       // cell?.backgroundColor = UIColor(red: 50/255, green: 62/255, blue: 78/255, alpha: 1)
            
    
            
            let chartObject = self.viewModel.dashBordDataArray[indexPath.row];
            
          
            
            let options = HIOptions()
            let chart = HIChart()
            chart.type = "column"
            
            let exporting = HIExporting();
            exporting.enabled = false;
            options.exporting = exporting
            options.chart = chart;
          
            let credits = HICredits()
            credits.enabled = false;
            options.credits = credits
          
            
            //chart.backgroundColor = HIColor(linearGradient: ["x1": 0, "y1": 0, "x2": 0, "y2": 300], stops: [[0, "rgb(29, 35, 48)"], [1, "rgb(128, 135, 232)"]])
            
           // chart.backgroundColor = HIColor.init(hexValue: "261e2331")
            chart.backgroundColor = HIColor.init(hexValue: "ffffff")
            
            
            
            let title = HITitle()
            title.text = chartObject.deptName
            
            let hiCSSObject = HICSSObject()
            hiCSSObject.color = "#fff"
            hiCSSObject.color = "#000"
            title.style = hiCSSObject
            
           
            
            
            let xaxis = HIXAxis()
            xaxis.type = "category"
            
            let yaxis = HIYAxis()
            yaxis.title = HITitle()
            
            if(indexPath.row == 0){
                yaxis.title.text = "Trucks"
            }else{
                yaxis.title.text = "Consignments"
            }
            yaxis.gridLineColor = HIColor.init(hexValue: "1e2331")
            
            let legend = HILegend()
            legend.enabled = NSNumber(value: false)
            
            let plotoptions = HIPlotOptions()
            plotoptions.series = HISeries()
          
    
            let onTimePecentage = Int((Double(chartObject.delayCounts.onTime*100)/Double(chartObject.totalActiveCount)).rounded())
            
            let delayedPecentage = Int((Double(chartObject.delayCounts.delayed*100)/Double(chartObject.totalActiveCount)).rounded())
            
            
            let criticalPecentage = Int((Double(chartObject.delayCounts.critical*100)/Double(chartObject.totalActiveCount)).rounded())
            
            
            let column = HIColumn()
            column.colorByPoint = NSNumber(value: true)
            column.data = [[
                "name": "Total",
                "y": NSNumber(value: chartObject.totalActiveCount),
                "drilldown": "Total"
                
                ],  [
                    "name": "On Time",
                    "y": NSNumber(value: chartObject.delayCounts.onTime),
                    "percentage": NSNumber(value: onTimePecentage),
                    "drilldown": "On Time"
                    
                ], [
                    "name": "Delayed",
                    "y": NSNumber(value: chartObject.delayCounts.delayed),
                    "percentage": NSNumber(value: delayedPecentage),
                    "drilldown": "Delayed"
                    
                ], [
                    "name": "Critical",
                    "y": NSNumber(value: chartObject.delayCounts.critical),
                    "percentage": NSNumber(value: criticalPecentage),
                    "drilldown": "Critical"
                    
                ]]
            
            
            let dataLabels = HIDataLabels();
            dataLabels.enabled = true;
           // dataLabels.format = "{point.y:1f}%"
             dataLabels.formatter  = HIFunction(jsFunction:"function(){ if(this.percentage == undefined){return (this.y)} else{ return (this.y)+ ' ('+this.percentage+'%)';}}")
            
            
            plotoptions.series.dataLabels = [dataLabels]
        
            
            let colors: [HIColor] = [
                HIColor(hexValue:"2d86fe"),
                HIColor(hexValue:"00cc82"),
                HIColor(hexValue:"f4e410"),
                HIColor(hexValue:"ff006d")
                ]
            column.colors = colors
         
            
            options.chart = chart
            options.title = title
            options.xAxis = [xaxis]
            options.yAxis = [yaxis]
            options.legend = legend
            options.plotOptions = plotoptions
            //options.tooltip = tooltip
            //options.drilldown = drilldown
            options.series = [column]
            
            
            
            let event = HIEvents()
            event.drilldown = HIFunction(closure: {  x in
                
                print(x?.getProperty("event.index") as Any, x?.getProperty("event.seriesOptions") as Any, x?.getProperty("event.point.x") as Any, x?.getProperty("event.point.y") as Any)
                let xIndex:Int = (x?.getProperty("event.point.x") as! Int)

                
                if(xIndex == 3){
                    let chartObject = self.viewModel.dashBordDataArray[indexPath.row]
       
                    let criticalListVC: CriticalListVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CriticalListVC") as! CriticalListVC
                    criticalListVC.deptCode = chartObject.deptCode
                    criticalListVC.deptName = chartObject.deptName
                    
                    criticalListVC.criticalTptList = chartObject.criticalTptList
                    self.navigationController?.pushViewController(criticalListVC, animated: true)
                }
                
                
            },properties: ["event.index", "event.seriesOptions", "event.point.x", "event.point.y"])
            chart.events = event
            
            chartView.options = options
            
            
           cell!.addSubview(chartView)
            
           // cell!.textLabel?.text = "Hi"
            return cell!
        }
        else {
            //self.updateCellButtonTag(cell!, newIndex: indexPath.row)
        }
       return cell!
    }
   
}
class DashBordVC: BaseViewController {
    var chartView: HIChartView!
    var viewModel: DashBoardViewModel!
    var chartsTableView: UITableView!
    
    func showAlert(alertMessage:String,title:String) {
        let alert = UIAlertController(title: title, message: alertMessage,         preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { _ in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        viewModel = DashBoardViewModel()
        
        self.title = "Dashboard"
        let screensize: CGRect = UIScreen.main.bounds
        let screenWidth = screensize.width
        let screenHeight = screensize.height
        
        chartsTableView = UITableView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight-40), style: .plain)
        chartsTableView.delegate = self
        chartsTableView.dataSource = self;
        chartsTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
       // self.chartsTableView.backgroundColor = UIColor(red: 29/255, green: 35/255, blue: 48/255, alpha: 1)
      //  self.view.backgroundColor = UIColor(red: 29/255, green: 35/255, blue: 48/255, alpha: 1)
        
        self.chartsTableView.backgroundColor = UIColor.white
        self.view.backgroundColor = UIColor.white
        
        self.view.addSubview(chartsTableView);
        
        //show loading Indicator
        DispatchQueue.main.async {
            LoadingOverlay.shared.showOverlay(view: UIApplication.shared.keyWindow!)
        }
  
        viewModel.fetchDashBordData(completion: { (isSuccess) in
            
            
            if isSuccess {
                //remove loading indcator
                DispatchQueue.main.async {
                    if self.viewModel.dashBordDataArray.count >= 3{
                        
                    
                    if(SharedObject.sharedInstance.userRoles.contains("admin") || SharedObject.sharedInstance.userRoles.contains("SITE_ADMIN") || SharedObject.sharedInstance.userRoles.contains("AD")){
                    }
                    else if SharedObject.sharedInstance.userRoles.contains("SND DDVM"){
                        for object in self.viewModel.dashBordDataArray {
                            if(object.deptName == "Sales & Dispatch"){
                                self.viewModel.dashBordDataArray.removeAll();
                                self.viewModel.dashBordDataArray.append(object);
                                break;
                            }
                        }
                    
                    
                    }else if SharedObject.sharedInstance.userRoles.contains("PRT DDVM"){
                        for object in self.viewModel.dashBordDataArray {
                            if(object.deptName == "Spare Parts (Courier)"){
                                self.viewModel.dashBordDataArray.removeAll();
                                self.viewModel.dashBordDataArray.append(object);
                                break;
                            }
                        }
                    }else{
                        self.viewModel.dashBordDataArray.removeAll()
                        self.showAlert(alertMessage: "You don't have roles, Please check with Admin for roles ", title:"Permissions")
                    }
                    self.chartsTableView.reloadData()
                    }
                }

                
               /* let object0 = self.viewModel.dashBordDataArray[0];
                print(object0.deptName)
                
                let object1 = self.viewModel.dashBordDataArray[1];
                print(object1.deptName)
                
                let object2 = self.viewModel.dashBordDataArray[2];
                print(object2.deptName)*/
                
            }else{
                self.showAlert(alertMessage: "Fail", title: "Dashboard")
                
            }
        })
        
    
       
    }
    
    
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setHidesBackButton(true, animated: true);

        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
    }

   /* if((x?.getProperty("event.point.x") == 3){
    /* let criticalListVC: CriticalListVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CriticalListVC") as! CriticalListVC
     self.navigationController?.pushViewController(criticalListVC, animated: true)*/
    }*/
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
