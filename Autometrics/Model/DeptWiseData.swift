//
//  DeptWiseData.swift
//  Autometrics
//
//  Created by Entrac Solutions on 03/06/20.
//  Copyright © 2020 Gangaraju. All rights reserved.
//

import UIKit



struct CriticalData: Codable {
    let consignee_city:String
    let consignee_name:String
    let consignee_code:String
    let consigner_code:String
    let consignment_code:String
    let delay_type:String
    let dept_code:String
    let dept_name:String
    let invoice_date:String
    let plant_location:String
    let spoc_contact_name:String
    let spoc_contact_number:String
    let status:String
    let transit_delay_days:String
    
    let transporter_name:String
    let transporters_name:String
    let uom:String
    let expected_trip_end:String
    let delay_reason:String
    
}


struct DeptWiseMessage: Codable {
    var critical_data:[CriticalData]
    let dept_code:String
    let dept_name:String
   
}

struct DeptmentWiseData: Codable {
    let message: [DeptWiseMessage]
    let status:String

}
