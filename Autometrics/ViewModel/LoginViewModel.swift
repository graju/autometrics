//
//  LoginViewModel.swift
//  Autometrics
//
//  Created by Gangaraju on 28/05/20.
//  Copyright © 2020 Gangaraju. All rights reserved.
//

import UIKit

struct LoginDatarequest: RequestType{
    var data: RequestData
    typealias ResponseType = LoginData
}

class LoginViewModel: NSObject {
    public var roles: [String] = []
    typealias FetchLoginDataCompletion = ((_ isSuccess: Bool) -> Void)

    func fetchLoginData(inputData:[String: Any?], completion: @escaping FetchLoginDataCompletion){
        
        let loginrequest = LoginDatarequest(data: RequestData(method: .post, url: url.login.rawValue,bodyParmaneters:inputData))
        
        loginrequest.serviceCall(
            onSuccess: { (loginResponseData: LoginData) in
                self.roles = loginResponseData.user.roles
                completion(true)
        },
            onError: { (error: Error) in
                completion(false)
                DispatchQueue.main.async {
                    LoadingOverlay.shared.hideOverlayView()
                    
                }
                switch error.localizedDescription {
                case ErrorMessages.noConnection.rawValue:
                    print("Error");
                //self.fetchOfflineData(completion: completion)
                default:
                    print(error.localizedDescription)
                }
                
                
        })
    }
}

